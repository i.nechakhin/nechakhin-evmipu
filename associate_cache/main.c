#include <stdio.h>
#include <stdlib.h>

void full_array (int* array, size_t size_cache, int count_frag) {
    int offset = 786432;
    for (int i = 0; i < (size_cache / count_frag); ++ i) {
        int next_index = i;
        for (int j = 0; j < count_frag; ++ j) {
            if (j == count_frag - 1) {
                array[next_index] = i + 1;
            } else {
                array[next_index] = next_index + offset;
                next_index += offset;
            }
        }
    }
}

double array_traversal (const int* array, size_t size_cache) {
    union ticks {
        unsigned long long t64;
        struct s32 { long th, tl; } t32;
    } start, end;
    //double cpu_Hz = 1992000000ULL;
    for (size_t k = 0, i = 0; i < size_cache; ++i) {
        k = array[k];
    }
    //size_t count_iter = 10;
    asm("rdtsc\n":"=a"(start.t32.th), "=d"(start.t32.tl));
    for (size_t k = 0, i = 0; i < size_cache; ++i) {
        k = array[k];
    }
    asm("rdtsc\n":"=a"(end.t32.th), "=d"(end.t32.tl));
    return (double) (end.t64 - start.t64) / (double) (size_cache);
}

void check_all_array_traversal () {
    puts("enter");
    size_t size_cache = 786432;
    int* array = calloc(size_cache * 32, sizeof(int));
    for (int i = 1; i <= 32; ++ i) {
        full_array(array, size_cache, i);
        double time = array_traversal(array, size_cache);
        printf("%f ", time);
        if (1 < 0) {
            puts("exit");
            return;
        }
    }
    printf("\n");
    free(array);
    puts("exit check_all_array_traversal");
}

int main (void) {
    check_all_array_traversal();
    return 0;
}
