cmake_minimum_required(VERSION 3.17)
project(cache_test_0)

set(CMAKE_CXX_STANDARD 17)

add_executable(cache_test_0 main.cpp)