#include <thread>
#include <iostream>
#include <string>
#include <ctime>

size_t return_size_array (int flag) {
    if (flag == 1) {
        return 4096;
    } else if (flag == 2) {
        return 32768;
    } else {
        return 393216;
    }
}

void add_in_arrayA (int* array, int index) {
    array[index] = index;
}

void add_in_arrayB (int* array, int index) {
    array[index] = 101;
}

int main () {
    int flag;
    std::cin >> flag;
    size_t size_array = return_size_array(flag);
    auto* array = (int*) malloc(size_array * sizeof(int));
    unsigned int start = clock();
    for (int i = 0; i < size_array / 2; ++ i) {
        std::thread tA(add_in_arrayA, array, 2 * i);
        std::thread tB(add_in_arrayB, array, 2 * i + 1);
        tA.join();
        tB.join();
    }
    unsigned int end = clock();
    std::cout << ((double) (end - start) / (double) size_array) << '\n';
    free(array);
    return 0;
}
