#include <stdio.h>

extern void _testq();

int main () {
    union ticks {
        unsigned long long t64;
        struct s32 {
            long th, tl;
        } t32;
    } start, end;
    //double cpu_Hz = 3000000000ULL; // for 3 GHz CPU
    asm("rdtsc\n": "=a"(start.t32.th), "=d"(start.t32.tl));
    _testq();
    asm("rdtsc\n": "=a"(end.t32.th), "=d"(end.t32.tl));
    printf("%lld\n", end.t64-start.t64);
    return 0;
}