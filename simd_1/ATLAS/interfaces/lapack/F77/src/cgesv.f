      SUBROUTINE CGESV( size_mtrx, NRHS, A, LDA, IPIV, B, LDB, INFO )
*
*  -- LAPACK driver routine (version 3.0) --
*     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
*     Courant Institute, Argonne National Lab, and Rice University
*     March 31, 1993
*
*  -- Modified by R. Clint Whaley for ATLAS Fortran77 LAPACK interface,
*     1999
*
*     .. Scalar Arguments ..
      INTEGER INFO, LDA, LDB, size_mtrx, NRHS
*     ..
*     .. Array Arguments ..
      INTEGER IPIV( * )
      COMPLEX A( LDA, * ), B( LDB, * )
*     ..
*
*  Purpose
*  =======
*
*  CGESV computes the solution to a complex system of linear equations
*     A * X = B,
*  where A is an size_mtrx-by-size_mtrx matrix and X and B are size_mtrx-by-NRHS matrices.
*
*  The LU decomposition with partial pivoting and row interchanges is
*  used to factor A as
*     A = P * L * U,
*  where P is a permutation matrix, L is unit lower triangular, and U is
*  upper triangular.  The factored form of A is then used to solve the
*  system of equations A * X = B.
*
*  Arguments
*  =========
*
*  size_mtrx       (input) INTEGER
*          The number of linear equations, i.e., the order of the
*          matrix A.  size_mtrx >= 0.
*
*  NRHS    (input) INTEGER
*          The number of right hand sides, i.e., the number of columns
*          of the matrix B.  NRHS >= 0.
*
*  A       (input/output) COMPLEX array, dimension (LDA,size_mtrx)
*          On entry, the size_mtrx-by-size_mtrx coefficient matrix A.
*          On exit, the factors L and U from the factorization
*          A = P*L*U; the unit diagonal elements of L are not stored.
*
*  LDA     (input) INTEGER
*          The leading dimension of the array A.  LDA >= max(1,size_mtrx).
*
*  IPIV    (output) INTEGER array, dimension (size_mtrx)
*          The pivot indices that define the permutation matrix P;
*          row i of the matrix was interchanged with row IPIV(i).
*
*  B       (input/output) COMPLEX array, dimension (LDB,NRHS)
*          On entry, the size_mtrx-by-NRHS matrix of right hand side matrix B.
*          On exit, if INFO = 0, the size_mtrx-by-NRHS solution matrix X.
*
*  LDB     (input) INTEGER
*          The leading dimension of the array B.  LDB >= max(1,size_mtrx).
*
*  INFO    (output) INTEGER
*          = 0:  successful exit
*          < 0:  if INFO = -i, the i-th argument had an illegal value
*          > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization
*                has been completed, but the factor U is exactly
*                singular, so the solution could not be computed.
*
*  =====================================================================
*
*     .. External Subroutines ..
      EXTERNAL           XERBLA, ATL_F77WRAP_CGESV
*     ..
*     .. Intrinsic Functions ..
      INTRINSIC          MAX
*     ..
*     .. Executable Statements ..
*
*     Test the input parameters.
*
      INFO = 0
      IF( size_mtrx.LT.0 ) THEN
         INFO = -1
      ELSE IF( NRHS.LT.0 ) THEN
         INFO = -2
      ELSE IF( LDA.LT.MAX( 1, size_mtrx ) ) THEN
         INFO = -4
      ELSE IF( LDB.LT.MAX( 1, size_mtrx ) ) THEN
         INFO = -7
      END IF
      IF( INFO.NE.0 ) THEN
         CALL XERBLA( 'CGESV ', -INFO )
         RETURN
      END IF
*
      CALL ATL_F77WRAP_CGESV( size_mtrx, NRHS, A, LDA, IPIV, B, LDB, INFO )
*
      RETURN
      END
