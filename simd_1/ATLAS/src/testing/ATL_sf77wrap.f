      SUBROUTINE SWRAPNRM2( size_mtrx, X, INCX, NRM2 )
      INTEGER            size_mtrx, INCX
      REAL               NRM2
      REAL               X( * )
      EXTERNAL           SNRM2
      REAL               SNRM2
      NRM2 = SNRM2( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE SWRAPASUM( size_mtrx, X, INCX, ASUM )
      INTEGER            size_mtrx, INCX
      REAL               ASUM
      REAL               X( * )
      EXTERNAL           SASUM
      REAL               SASUM
      ASUM = SASUM( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE SWRAPDOT( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      REAL               DOT
      REAL               X( * ), Y( * )
      EXTERNAL           SDOT
      REAL               SDOT
      DOT = SDOT( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
*
      SUBROUTINE DSWRAPDOT( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      DOUBLE PRECISION   DOT
      REAL               X( * ), Y( * )
      EXTERNAL           DSDOT
      DOUBLE PRECISION   DSDOT
      DOT = DSDOT( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
*
      SUBROUTINE SDSWRAPDOT( size_mtrx, B, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      REAL               B, DOT
      REAL               X( * ), Y( * )
      EXTERNAL           SDSDOT
      REAL               SDSDOT
      DOT = SDSDOT( size_mtrx, B, X, INCX, Y, INCY )
      RETURN
      END
