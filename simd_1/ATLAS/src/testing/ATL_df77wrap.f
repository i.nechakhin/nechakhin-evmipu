      SUBROUTINE DWRAPNRM2( size_mtrx, X, INCX, NRM2 )
      INTEGER            size_mtrx, INCX
      DOUBLE PRECISION   NRM2
      DOUBLE PRECISION   X( * )
      EXTERNAL           DNRM2
      DOUBLE PRECISION   DNRM2
      NRM2 = DNRM2( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE DWRAPASUM( size_mtrx, X, INCX, ASUM )
      INTEGER            size_mtrx, INCX
      DOUBLE PRECISION   ASUM
      DOUBLE PRECISION   X( * )
      EXTERNAL           DASUM
      DOUBLE PRECISION   DASUM
      ASUM = DASUM( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE DWRAPDOT( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      DOUBLE PRECISION   DOT
      DOUBLE PRECISION   X( * ), Y( * )
      EXTERNAL           DDOT
      DOUBLE PRECISION   DDOT
      DOT = DDOT( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
