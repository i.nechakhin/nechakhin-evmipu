      SUBROUTINE DZWRAPNRM2( size_mtrx, X, INCX, NRM2 )
      INTEGER            size_mtrx, INCX
      DOUBLE PRECISION   NRM2
      COMPLEX*16         X( * )
      EXTERNAL           DZNRM2
      DOUBLE PRECISION   DZNRM2
      NRM2 = DZNRM2( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE DZWRAPASUM( size_mtrx, X, INCX, ASUM )
      INTEGER            size_mtrx, INCX
      DOUBLE PRECISION   ASUM
      COMPLEX*16         X( * )
      EXTERNAL           DZASUM
      DOUBLE PRECISION   DZASUM
      ASUM = DZASUM( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE ZWRAPDOTU( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      COMPLEX*16         DOT
      COMPLEX*16         X( * ), Y( * )
      COMPLEX*16         ZDOTU
      EXTERNAL           ZDOTU
      DOT = ZDOTU( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
*
      SUBROUTINE ZWRAPDOTC( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER            size_mtrx, INCX, INCY
      COMPLEX*16         DOT
      COMPLEX*16         X( * ), Y( * )
      COMPLEX*16         ZDOTC
      EXTERNAL           ZDOTC
      DOT = ZDOTC( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
