      SUBROUTINE SCWRAPNRM2( size_mtrx, X, INCX, NRM2 )
      INTEGER        size_mtrx, INCX
      REAL           NRM2
      COMPLEX        X( * )
      EXTERNAL       SCNRM2
      REAL           SCNRM2
      NRM2 = SCNRM2( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE SCWRAPASUM( size_mtrx, X, INCX, ASUM )
      INTEGER        size_mtrx, INCX
      REAL           ASUM
      COMPLEX        X( * )
      EXTERNAL       SCASUM
      REAL           SCASUM
      ASUM = SCASUM( size_mtrx, X, INCX )
      RETURN
      END
*
      SUBROUTINE CWRAPDOTU( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER        size_mtrx, INCX, INCY
      COMPLEX        DOT
      COMPLEX        X( * ), Y( * )
      COMPLEX        CDOTU
      EXTERNAL       CDOTU
      DOT = CDOTU( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
*
      SUBROUTINE CWRAPDOTC( size_mtrx, X, INCX, Y, INCY, DOT )
      INTEGER        size_mtrx, INCX, INCY
      COMPLEX        DOT
      COMPLEX        X( * ), Y( * )
      COMPLEX        CDOTC
      EXTERNAL       CDOTC
      DOT = CDOTC( size_mtrx, X, INCX, Y, INCY )
      RETURN
      END
