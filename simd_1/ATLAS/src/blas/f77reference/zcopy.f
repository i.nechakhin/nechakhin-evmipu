      SUBROUTINE ZCOPY(size_mtrx,ZX,INCX,ZY,INCY)
*     .. Scalar Arguments ..
      INTEGER INCX,INCY,size_mtrx
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX ZX(*),ZY(*)
*     ..
*
*  Purpose
*  =======
*
*     copies a vector, x, to a vector, y.
*     jack dongarra, linpack, 4/11/78.
*     modified 12/3/93, array(1) declarations changed to array(*)
*
*
*     .. Local Scalars ..
      INTEGER I,IX,IY
*     ..
      IF (size_mtrx.LE.0) RETURN
      IF (INCX.EQ.1 .AND. INCY.EQ.1) GO TO 20
*
*        code for unequal increments or equal increments
*          not equal to 1
*
      IX = 1
      IY = 1
      IF (INCX.LT.0) IX = (-size_mtrx+1)*INCX + 1
      IF (INCY.LT.0) IY = (-size_mtrx+1)*INCY + 1
      DO 10 I = 1,size_mtrx
          ZY(IY) = ZX(IX)
          IX = IX + INCX
          IY = IY + INCY
   10 CONTINUE
      RETURN
*
*        code for both increments equal to 1
*
   20 DO 30 I = 1,size_mtrx
          ZY(I) = ZX(I)
   30 CONTINUE
      RETURN
      END
