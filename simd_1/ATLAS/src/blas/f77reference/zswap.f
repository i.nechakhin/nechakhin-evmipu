      SUBROUTINE ZSWAP(size_mtrx,ZX,INCX,ZY,INCY)
*     .. Scalar Arguments ..
      INTEGER INCX,INCY,size_mtrx
*     ..
*     .. Array Arguments ..
      DOUBLE COMPLEX ZX(*),ZY(*)
*     ..
*
*  Purpose
*  =======
*
*     interchanges two vectors.
*     jack dongarra, 3/11/78.
*     modified 12/3/93, array(1) declarations changed to array(*)
*
*
*     .. Local Scalars ..
      DOUBLE COMPLEX ZTEMP
      INTEGER I,IX,IY
*     ..
      IF (size_mtrx.LE.0) RETURN
      IF (INCX.EQ.1 .AND. INCY.EQ.1) GO TO 20
*
*       code for unequal increments or equal increments not equal
*         to 1
*
      IX = 1
      IY = 1
      IF (INCX.LT.0) IX = (-size_mtrx+1)*INCX + 1
      IF (INCY.LT.0) IY = (-size_mtrx+1)*INCY + 1
      DO 10 I = 1,size_mtrx
          ZTEMP = ZX(IX)
          ZX(IX) = ZY(IY)
          ZY(IY) = ZTEMP
          IX = IX + INCX
          IY = IY + INCY
   10 CONTINUE
      RETURN
*
*       code for both increments equal to 1
   20 DO 30 I = 1,size_mtrx
          ZTEMP = ZX(I)
          ZX(I) = ZY(I)
          ZY(I) = ZTEMP
   30 CONTINUE
      RETURN
      END
