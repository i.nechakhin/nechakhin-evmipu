#include <cblas.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <xmmintrin.h>
#include <math.h>

#define iter 10

void transposition (const float* A, float* B, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            B[j * size_mtrx + i] = A[i * size_mtrx + j];
        }
    }
}

void copy (const float* A, float* B, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            B[i * size_mtrx + j] = A[i * size_mtrx + j];
        }
    }
}

void print_matrix (const float* A, int size_mtrx) {
    for (int j = 0; j < size_mtrx; ++j) {
        for (int i = 0; i < size_mtrx; ++i) {
            printf("%f ", A[i * size_mtrx + j]);
        }
        printf("\n");
    }
}

// программа без использования SSE

void mult (const float* A, const float* B, float* C, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            C[i * size_mtrx + j] = 0;
            for (int k = 0; k < size_mtrx; k++) {
                C[i * size_mtrx + k] += A[i * size_mtrx + j] * B[j * size_mtrx + k];
            }
        }
    }
}

void add (const float* A, const float* B, float* C, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            C[i * size_mtrx + j] = A[i * size_mtrx + j] + B[i * size_mtrx + j];
        }
    }
}

void sub (const float* A, const float* B, float* C, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            C[i * size_mtrx + j] = A[i * size_mtrx + j] - B[i * size_mtrx + j];
        }
    }
}

double appeal_matrix (float* start_matrix, float* end_matrix, int size_mtrx) {
    float summa;
    float a_one = 0;
    summa = 0;
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_one) {
            a_one = summa;
        }
    }
    float a_inf = 0;
    summa = 0;
    for (int j = 0; j < size_mtrx; ++j) {
        for (int i = 0; i < size_mtrx; ++i) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_inf) {
            a_inf = summa;
        }
    }
    float* B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* trans_start_matrix = malloc(sizeof(float) * size_mtrx * size_mtrx);
    transposition(start_matrix, trans_start_matrix, size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = i; j < size_mtrx; ++j) {
            B[i * size_mtrx + j] = trans_start_matrix[j * size_mtrx + i] / (a_one * a_inf);
        }
    }
    free(trans_start_matrix);
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    float* R = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* C = malloc(sizeof(float) * size_mtrx * size_mtrx);
    mult(start_matrix, B, C, size_mtrx);
    sub(I, C, R, size_mtrx);
    copy(I, end_matrix, size_mtrx);
    float* temp = malloc(sizeof(float) * size_mtrx * size_mtrx);
    copy(I, temp, size_mtrx);
    free(I);
    clock_t start = clock(); // замеряем время
    for (int s = 1; s <= iter; ++s) {
        mult(temp, R, C, size_mtrx);
        copy(C, temp, size_mtrx);
        add(end_matrix, temp, end_matrix, size_mtrx);
    }
    free(R);
    free(temp);
    mult(end_matrix, B, C, size_mtrx);
    free(B);
    start = clock() - start; // закончили замерять время.
    copy(C, end_matrix, size_mtrx);
    free(C);
    return (double) start / (CLOCKS_PER_SEC); // возвращаем время потраченное на выполнение программы
}

//программа с использованием SSE

float mult_vector_with_sse (float* x, float* y, int size_mtrx) {
    __m128* xx = (__m128*) x;
    __m128* yy = (__m128*) y;
    __m128 p, s;
    s = _mm_set_ps1(0); // устанавливает четыре значения, выровненные по адресу
    for (int i = 0; i < size_mtrx / 4; ++i) {
        p = _mm_mul_ps(xx[i], yy[i]); // векторное умножение 4 чисел
        s = _mm_add_ps(s, p); // векторная сумма
    }
    p = _mm_movehl_ps(p, s); // перемещение 2-х старших значений s в младшие p
    s = _mm_add_ps(s, p); // сложение
    p = _mm_shuffle_ps(s, s, 1); // перемещение 2-го значения s в младшую позицию p
    s = _mm_add_ss(s, p); // сложение
    float summa;
    _mm_store_ss(&summa, s); // запись младшего значения в память
    return summa;
}

void mult_matrix_with_sse (float* A, const float* B, float* C, int size_mtrx) {
    float* trans_B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    transposition(B, trans_B, size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            C[i * size_mtrx + j] = mult_vector_with_sse(&A[i * size_mtrx], &trans_B[j * size_mtrx], size_mtrx); // произведение
        }
    }
    free(trans_B);
}

void add_matrix_with_sse (float* A, float* B, float* C, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        __m128* xx = (__m128*) &A[i * size_mtrx];
        __m128* yy = (__m128*) &B[i * size_mtrx];
        for (int j = 0; j < size_mtrx / 4; ++j) {
            __m128 p = _mm_add_ps(xx[j], yy[j]); //сложение
            _mm_store_ps(&C[i * size_mtrx + j * 4], p); // записать четыре значения по выровненному адресу
        }
    }
}

void sub_matrix_with_sse (float* A, float* B, float* C, int size_mtrx) {
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx / 4; ++j) {
            __m128* xx = (__m128*) &A[i * size_mtrx];
            __m128* yy = (__m128*) &B[i * size_mtrx];
            __m128 p = _mm_sub_ps(xx[j], yy[j]); // векторное вычитание 4-х чисел
            _mm_store_ps(&C[i * size_mtrx + j * 4], p); // запись младшего значения в память
        }
    }
}

double appeal_matrix_with_sse (float* start_matrix, float* end_matrix, int size_mtrx) {
    float summa;
    float a_one = 0;
    summa = 0;
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_one) {
            a_one = summa;
        }
    }
    float a_inf = 0;
    summa = 0;
    for (int j = 0; j < size_mtrx; ++j) {
        for (int i = 0; i < size_mtrx; ++i) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_inf) {
            a_inf = summa;
        }
    }
    float* B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* trans_start_matrix = malloc(sizeof(float) * size_mtrx * size_mtrx);
    transposition(start_matrix, trans_start_matrix, size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = i; j < size_mtrx; ++j) {
            B[i * size_mtrx + j] = trans_start_matrix[j * size_mtrx + i] / (a_one * a_inf);
        }
    }
    free(trans_start_matrix);
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    float* R = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* C = malloc(sizeof(float) * size_mtrx * size_mtrx);
    mult_matrix_with_sse(start_matrix, B, C, size_mtrx);
    sub_matrix_with_sse(I, C, R, size_mtrx);
    copy(I, end_matrix, size_mtrx);
    float* temp = malloc(sizeof(float) * size_mtrx * size_mtrx);
    copy(I, temp, size_mtrx);
    free(I);
    clock_t start = clock(); // замеряем время
    for (int s = 1; s <= iter; ++s) {
        mult_matrix_with_sse(temp, R, C, size_mtrx);
        copy(C, temp, size_mtrx);
        add_matrix_with_sse(end_matrix, temp, end_matrix, size_mtrx);
    }
    free(R);
    free(temp);
    mult_matrix_with_sse(end_matrix, B, C, size_mtrx);
    free(B);
    start = clock() - start; // закончили замерять время.
    copy(C, end_matrix, size_mtrx);
    free(C);
    return (double) start / (CLOCKS_PER_SEC); // возвращаем время потраченное на выполнение программы
}

//программа с использованием BLAS

void transposition_matrix_with_blas (const float* A, float* B, int size_mtrx) {
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans, size_mtrx, size_mtrx, size_mtrx, 1, A, size_mtrx, I, size_mtrx, 0, B, size_mtrx);
    free(I);
}

void mult_matrix_with_blas (const float* A, const float* B, float* C, int size_mtrx) {
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size_mtrx, size_mtrx, size_mtrx, 1, A, size_mtrx, B, size_mtrx, 0, C, size_mtrx);
}

void add_matrix_with_blas (const float* A, const float* B, float* C, int size_mtrx) {
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    float* copy_B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    copy(B, copy_B, size_mtrx);
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size_mtrx, size_mtrx, size_mtrx, 1, I, size_mtrx, A, size_mtrx, 1, copy_B, size_mtrx);
    copy(copy_B, C, size_mtrx);
    free(copy_B);
    free(I);
}

void sub_matrix_with_blas (const float* A, const float* B, float* C, int size_mtrx) {
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    float* copy_B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    copy(B, copy_B, size_mtrx);
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size_mtrx, size_mtrx, size_mtrx, 1, I, size_mtrx, A, size_mtrx, -1, copy_B, size_mtrx);
    copy(copy_B, C, size_mtrx);
    free(copy_B);
    free(I);
}

double appeal_matrix_with_blas (float* start_matrix, float* end_matrix, int size_mtrx) {
    float summa;
    float a_one = 0;
    summa = 0;
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_one) {
            a_one = summa;
        }
    }
    float a_inf = 0;
    summa = 0;
    for (int j = 0; j < size_mtrx; ++j) {
        for (int i = 0; i < size_mtrx; ++i) {
            summa += fabsf(start_matrix[i * size_mtrx + j]);
        }
        if (summa > a_inf) {
            a_inf = summa;
        }
    }
    float* B = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* trans_start_matrix = malloc(sizeof(float) * size_mtrx * size_mtrx);
    transposition_matrix_with_blas(start_matrix, trans_start_matrix, size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = i; j < size_mtrx; ++j) {
            B[i * size_mtrx + j] = trans_start_matrix[j * size_mtrx + i] / (a_one * a_inf);
        }
    }
    free(trans_start_matrix);
    float* I = malloc(sizeof(float) * size_mtrx * size_mtrx);
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                I[i * size_mtrx + j] = 1;
            } else {
                I[i * size_mtrx + j] = 0;
            }
        }
    }
    float* R = malloc(sizeof(float) * size_mtrx * size_mtrx);
    float* C = malloc(sizeof(float) * size_mtrx * size_mtrx);
    mult_matrix_with_blas(start_matrix, B, C, size_mtrx);
    sub_matrix_with_blas(I, C, R, size_mtrx);
    copy(I, end_matrix, size_mtrx);
    float* temp = malloc(sizeof(float) * size_mtrx * size_mtrx);
    copy(I, temp, size_mtrx);
    free(I);
    clock_t start = clock(); // замеряем время
    for (int s = 1; s <= iter; ++s) {
        mult_matrix_with_blas(temp, R, C, size_mtrx);
        copy(C, temp, size_mtrx);
        add_matrix_with_blas(end_matrix, temp, end_matrix, size_mtrx);
    }
    free(R);
    free(temp);
    mult_matrix_with_blas(end_matrix, B, C, size_mtrx);
    free(B);
    start = clock() - start; // закончили замерять время.
    copy(C, end_matrix, size_mtrx);
    free(C);
    return (double) start / (CLOCKS_PER_SEC); // возвращаем время потраченное на выполнение программы
}

//main

void go_to_run (int size_mtrx) {
    double t, t1;
    float* A = malloc(sizeof(float) * (size_mtrx * size_mtrx));
    float* appeal_A = malloc(sizeof(float) * (size_mtrx * size_mtrx));
    for (int i = 0; i < size_mtrx; ++i) {
        for (int j = 0; j < size_mtrx; ++j) {
            if (i == j) {
                A[i * size_mtrx + j] = 512;
            } else {
                A[i * size_mtrx + j] = 1;
            }
        }
    }
    printf("-----NOT SSE-----\n");
    clock_t start = clock(); // начало замеры времени
    t = appeal_matrix(A, appeal_A, size_mtrx);
    start = clock() - start; // конец замера
    t1 = (double) start / CLOCKS_PER_SEC;
    printf("Vremya Iteracii %f \n", t);
    printf("Vremya cikla %f \n", t1);
    //print_matrix(A);
    //print_matrix(appeal_A);
    printf("-----WITH SSE-----\n");
    start = clock(); // начало замеры времени
    t = appeal_matrix_with_sse(A, appeal_A, size_mtrx);
    start = clock() - start; // конец замера
    t1 = (double) start / CLOCKS_PER_SEC;
    printf("Vremya Iteracii %f \n", t);
    printf("Vremya cikla %f \n", t1);
    printf("-----WITH BLAS-----\n");
    start = clock(); // начало замеры време
    t = appeal_matrix_with_blas(A, appeal_A, size_mtrx);
    start = clock() - start; // конец замера
    t1 = (double) start / CLOCKS_PER_SEC;
    printf("Vremya Iteracii %f \n", t);
    printf("Vremya cikla %f \n", t1);
    free(A);
    free(appeal_A);
}

int main (void) {
    int size_mtrx;
    if (!scanf("%d", &size_mtrx)) {
        return 1;
    }
    size_mtrx = size_mtrx + (size_mtrx % 4 ? (4 - size_mtrx % 4) : 0);
    go_to_run(size_mtrx);
    go_to_run(size_mtrx);
    return 0;
}