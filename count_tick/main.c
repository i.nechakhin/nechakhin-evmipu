#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void straight_full_array (int* array, size_t size_array) {
    for (size_t i = 0; i < size_array; ++i) {
        array[i] = (int) i + 1;
    }
    array[size_array - 1] = 0;
}

void back_full_array (int* array, size_t size_array) {
    array[0] = (int) size_array - 1;
    for (size_t i = 1; i < size_array; ++i) {
        array[i] = (int) i - 1;
    }
}
/*
void random_full_array (int* array, size_t size_array) {
    int temp = 0;
    for (size_t i = 0; i < size_array / 3 * 3; ++ i) {
        temp += ((i % 3 == 0) ? 3 : 0);
        if (i % 3 == 0) {
            array[i] = temp + 2;
        } else if (i % 3 == 1) {
            array[i] = temp - 3;
        } else {
            array[i] = temp - 2;
        }
    }
    if (size_array % 3 == 0) {
        array[size_array - 3] = 2;
    } else if (size_array % 3 == 1) {
        array[size_array - 4] = (int) size_array - 1;
        array[size_array - 1] = 2;
    } else {
        array[size_array - 5] = (int) size_array - 1;
        array[size_array - 1] = (int) size_array - 2;
        array[size_array - 2] = 2;
    }
}

int is_elem_in_array (int elem, const int* array, size_t size_array) {
    for (int i = 0; i < size_array; ++ i) {
        if (array[i] == elem) {
            return 1;
        }
    }
    return 0;
}

void random_full_array (int* array, size_t size_array) {
    for (int i = 0; i < size_array; ++ i) {
        int num = rand() % size_array;
        while (!is_elem_in_array(num, array, size_array)) {
            num = rand() % size_array;
        }
        array[i] = num;
    }
}
*/
void swap (int* elem1, int* elem2) {
    int temp = *elem1;
    *elem1 = *elem2;
    *elem2 = temp;
}

void random_full_array (int* array, size_t size_array) {
    for (size_t i = 0; i < size_array; ++i) {
        array[i] = (int) i;
    }
    for (size_t i = 0; i < size_array; ++i) {
        int j = rand() % size_array;
        swap(&array[i], &array[j]);
    }
}

double array_traversal (const int* array, size_t size_array) {
    union ticks {
        unsigned long long t64;
        struct s32 { long th, tl; } t32;
    } start, end;
    //double cpu_Hz = 1992000000ULL;
    for (size_t k = 0, i = 0; i < size_array; ++i) {
        k = array[k];
    }
    size_t count_iter = 10;
    asm("rdtsc\n":"=a"(start.t32.th), "=d"(start.t32.tl));
    for (size_t k = 0, i = 0; i < size_array * count_iter; ++i) {
        k = array[k];
    }
    asm("rdtsc\n":"=a"(end.t32.th), "=d"(end.t32.tl));
    return (double) (end.t64 - start.t64) / (double) (size_array * count_iter);
}

double repeat_for_the_best_time (const int* array, size_t size_array) {
    double time = array_traversal(array, size_array);
    for (int i = 0; i < 10; ++ i) {
        double new_time = array_traversal(array, size_array);
        if (new_time < time) {
            time = new_time;
        }
    }
    return time;
}

/*
double array_traversal (const int* array, size_t size_array) {
    for (size_t k = 0, i = 0; i < size_array; ++i) {
        k = array[k];
    }
    size_t count_iter = 10;
    clock_t start = clock();
    for (size_t k = 0, i = 0; i < size_array * count_iter; ++i) {
        k = array[k];
    }
    start = clock() - start;
    return (double) (start) / (double) (size_array * count_iter);
}
*/
void check_all_array_traversal (const int flag) {
    for (size_t i = 256; i != 8388608 * 2; i *= 2) {
        size_t size_array = i;
        int* array = malloc(size_array * sizeof(int));
        if (flag == 1) {
            straight_full_array(array, size_array);
        } else if (flag == 2) {
            back_full_array(array, size_array);
        } else {
            random_full_array(array, size_array);
        }
        double time = repeat_for_the_best_time(array, size_array);
        printf("%f ", time);
        free(array);
    }
    printf("\n");
}

void print_array (const int* array, size_t size_array) {
    for (size_t i = 0; i < size_array; ++i) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main () {
    check_all_array_traversal(1);
    check_all_array_traversal(2);
    check_all_array_traversal(3);
    return 0;
}
