#include <thread>
#include <iostream>
#include <string>
#include <ctime>

size_t return_size_array (int flag) {
    if (flag == 1) {
        return 4096;
    } else if (flag == 2) {
        return 32768;
    } else {
        return 393216;
    }
}

void runA (int* array, size_t size_array) {
    unsigned int start = clock();
    for (int i = 0; i < size_array; i = i + 2) {
        array[i] = i;
    }
    for (int i = 0; i < size_array; i = i + 2) {
        array[i] = i;
    }
    for (int i = 0; i < size_array; i = i + 2) {
        array[i] = i;
    }
    unsigned int end = clock();
    std::cout << "1 potok" << ((double) (end - start) / (double) size_array * 3) << '\n';
}

void runB (int* array, size_t size_array) {
    unsigned int start = clock();
    for (int i = 1; i < size_array; i = i + 2) {
        array[i] = i + 101;
    }
    for (int i = 1; i < size_array; i = i + 2) {
        array[i] = i + 101;
    }
    for (int i = 1; i < size_array; i = i + 2) {
        array[i] = i + 101;
    }
    unsigned int end = clock();
    std::cout << "2 potok" << ((double) (end - start) / (double) size_array * 3) << '\n';
}

void print_array (int* array, size_t size_array) {
    for (int i = 0; i < size_array; ++ i) {
        std::cout << array[i] << " ";
    }
}

void run (int flag) {
    size_t size_array = return_size_array(flag);
    auto* array = (int*) malloc(size_array * sizeof(int));
    std::thread tA(runA, array, size_array);
    std::thread tB(runB, array, size_array);
    tA.join();
    tB.join();
    free(array);
}

int main () {
    int flag;
    std::cin >> flag;
    run(flag);
    run(flag);
    run(flag);
    run(flag);
    run(flag);
    return 0;
}